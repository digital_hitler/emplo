'use strict';

/**
 * Employee list page controller (default page)
 * @param $scope
 * @param employeeListLocalStorageProvider
 * @param $location
 */

module.exports = function ($scope, employeeListLocalStorageProvider, $location) {

  // Binding local scope
  // Items list:
  $scope.list = employeeListLocalStorageProvider;

  // Items count:
  $scope.total = Object.keys($scope.list.items).length;

  // Hide list if routing somewhere else:
  $scope.$on('$routeChangeSuccess', function() {
    var path = $location.path();
    if (path === '/'){
      $scope.show = true;
      $('#employeeListContainer').css('opacity', 1);
    } else {
      $scope.show = false;
    }
  });

  /**
   * Show create employee view
   */
  $scope.showCreate = function() {
    $location.path('/create');
  }

  /**
   * Show edit employee view
   * @param {string} id employee ID
   */
  $scope.showEdit = function(id) {
    $location.path('/edit/' + id);
  }


};