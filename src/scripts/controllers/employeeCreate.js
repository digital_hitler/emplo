'use strict';

/**
 * Create new employee page controller
 * @param $scope
 * @param $routeParams
 * @param $location
 * @param employeeListLocalStorageService
 */

module.exports = function($scope, $routeParams, $location, employeeListLocalStorageService) {

  $scope.reset = function() {
    $scope.item = {
      firstName: '',
      lastName: '',
      address: '',
      dateJoined: '',
      birthDate: ''
    };
  };

  $scope.dismiss = function() {
    $location.path('/');
  }

  /**
   * Create new employee and close window
   * @param item
   * @param employeeForm
   */
  $scope.save = function(isValid) {
    if(isValid) {
      let createdId = employeeListLocalStorageService.create($scope.item);
      if(typeof createdId === 'string') {
        $scope.dismiss();
      }
    } else {
      alert('Пожалуйста, заполните все поля.\n Пожалуйста, простите за алерт.');
    }
  }

  $scope.reset();
  $scope.title = 'Новый сотрудник';
  $('#modalFormDateJoined').datepicker({
    format: "dd.mm.yyyy",
    clearBtn: true,
    autoclose: true,
    immediateUpdates: true,
    toggleActive: true,
    todayHighlight: true,
    language: 'ru'
  });

}