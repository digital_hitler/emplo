'use strict';
/**
 * Edit employee page controller
 * @param $scope
 * @param $routeParams
 * @param $location
 * @param employeeListLocalStorageService
 */

module.exports = function($scope, $routeParams, $location, employeeListLocalStorageService) {

  /**
   * Go back withou saving changes
   */
  $scope.dismiss = function() {
    $location.path('/');
    $scope.item = item;
  }

  /**
   * Save changes and go back
   * @param isValid form validity flag
   */
  $scope.save = function(isValid) {
    if(isValid) {
      let createdId = employeeListLocalStorageService.update($scope.item);
      if(typeof createdId === 'string') {
        $scope.dismiss();
      }
    } else {
      alert('Пожалуйста, заполните все поля.\n Пожалуйста, простите за алерт.');
    }
  }

  // <h2> title
  $scope.title = 'Редактирование сотрудника';

  // Looking for editable item
  var item = employeeListLocalStorageService.getById($routeParams.id);

  // If found
  if(item) {

    // Filling scope
    $scope.item = angular.copy(item);

    // Configuring datepicker
    var tmpDateJoined = new Date($scope.item.dateJoined * 1000);
    $('#modalFormDateJoined').datepicker({
      format: "dd.mm.yyyy",
      autoclose: true,
      defaultViewDate: {
        year: tmpDateJoined.getFullYear(),
        month: tmpDateJoined.getMonth(),
        day: tmpDateJoined.getDay()
      },
      todayHighlight: true,
      language: 'ru'
    });
    $('#modalFormDateJoined').datepicker('setDate', $('#modalFormDateJoined').val());

    // If item isn't found (eg. nothing to edit)
    // redirect to main page
  } else {
    $scope.dismiss();
  }

}