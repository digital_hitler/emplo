'use strict';


/**
 * Entity class
 * E56-styled class that can be used to extending by models
 * in case of schema-validation required.
 */

class Entity {

  /**
   * Trying to write properties to the class instance.
   * Will be written if all properties are valid by schema
   *
   * See `../models/Employee` for example
   * @param fields      properties
   * @param schema      schema
   * @returns {boolean} result of processing
   */
  applyFields (fields, schema) {
    if (fields && typeof fields === 'object' && schema && typeof schema === 'object') {
      let keys = Object.keys(fields);
      for(let key in schema) {
        let val = fields[key];
        this[key] = this.applyField(key, val, schema[key]);
        if(this[key] === false) {
          return false;
        }
      }
      return true;
    } else {
      console.error(
          'Wrong data given: ', fields,
          'Defined schema: ', schema
      );
      return false;
    }
  }

  /**
   * Checks field to the schema rules & appends it to the child class instance if alright.
   * @param fieldName
   * @param fieldValue
   * @param fieldSchema
   * @returns {*}
   */
  applyField(fieldName, fieldValue, fieldSchema) {
    let fieldType     = fieldSchema[0] || 'string',
        fieldRequired = fieldSchema[1] || false,
        fieldDefault  = fieldSchema[2] || '';
    let result = fieldValue;
    if(fieldName && typeof fieldName === 'string') {
      if(fieldRequired === true && !fieldValue) {
        // Field value required but not set
        return false;
      } else {
        if(!fieldValue) {
          if(typeof fieldDefault === 'function') {
            result = fieldDefault();
          } else {
            result = fieldDefault;
          }
        }
      }

      if(fieldType === 'string') {
        result += '';
      }

      return result;
    } else return false;
  }

  /**
   * Constructor.
   * Must be called via `super(fields, schema)` in child classes.
   * @param fields
   * @param schema
   */
  constructor(fields, schema) {
    if(this.applyFields(fields, schema)) {
      this.__properEntity = true;
    } else {
      this.__properEntity = false;
      throw new Error("Wrong entity. Check fields");
    }
  }
}

module.exports = Entity;