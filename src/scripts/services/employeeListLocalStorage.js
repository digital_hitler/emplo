'use strict';

var employeeListModel = require('../models/employeeList');
var Employee = require('../models/Employee');

module.exports = function(localStorageService, $rootScope) {
  this.getList = function() {
    return this.empListStored;
  };

  this.getById = function(id) {
    return this.empListStored.items[id] || false;
  };

  /**
   * Appends new record and commits it
   * @todo: Sanitize input
   * @param {object} fields Employee prototype fields
   */
  this.create = function(fields) {
    let NewEmployee = new Employee(fields);
    if(NewEmployee && typeof NewEmployee.id === 'string' && NewEmployee.__properEntity === true) {
      this.pull();
      this.empListStored.items[NewEmployee.id] = NewEmployee;
      this.commit();
      return NewEmployee.id;
    } else {
      return false;
    }
  }

  /**
   * Updates existing record.
   * @todo: This method is already equals create. Needs to merge both-used code.
   * @param {object} fields Employee fields
   */
  this.update = function(item) {
    if(!this.empListStored.items[item.id] instanceof Employee) return false;

    let editedEmployee = new Employee(item);
    if(editedEmployee && typeof editedEmployee.id === 'string' && editedEmployee.__properEntity === true) {
      this.empListStored.items[editedEmployee.id] = editedEmployee;
      this.commit();
      return editedEmployee.id;
    } else return false;
  }

  /**
   * Write current service data to localStorage from memory.
   */
  this.commit = function() {
    console.log(this);
    localStorageService.set('empList', {
      items: this.empListStored.items
    });
    console.log('localStorageService: commited ' + Object.keys(this.empListStored.items).length + ' item(s) to' +
        ' localStorage');
  };

  /**
   * Read stored data.
   * This will automatically fill & commit data to localStorage
   * if no data collection will be found (eg. first run)
   */
  this.pull = function() {
    if(localStorageService.isSupported) {
      var lsData = localStorageService.get('empList');
      if(lsData && lsData.items) {
        this.empListStored = employeeListModel;
        this.empListStored.items = {};
        for(var curr in lsData.items) {
          let key = lsData.items[curr]['id'];
          this.empListStored.items[key] = new employeeListModel.itemModel(lsData.items[curr]);
        }
        console.log('Pulled ' + Object.keys(this.empListStored.items).length + ' item(s) from localStorage');
      }
    }
  }

  /**
   * App data initialization
   */
  if(localStorageService.isSupported) {
    this.pull();
    if(!this.empListStored) {
      console.info('Empty localStorage data detected. We are going to fill it with defaults.');
      this.empListStored = $rootScope.defaultData;
      this.commit();
    }
  } else {
    console.warning('LocalStorage is not available in your browser. Data will display properly but cannot be saved');
    this.empListStored = $rootScope.defaultData;
  }

  console.log("Stored data:", this.empListStored);

  return this;
};