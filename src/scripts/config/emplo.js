'use strict';
/**
 * App main configuration
 * Setting prefix for localStorage service &
 * binding routes
 */
module.exports = function (localStorageServiceProvider, $routeProvider) {
  localStorageServiceProvider.setPrefix('emplo');
  $routeProvider
      // create employee route
      .when('/create', {
        templateUrl: '/templates/employee.html',
        controller: 'employeeCreateController'
      })

      // edit employee route
      .when('/edit/:id', {
        templateUrl: '/templates/employee.html',
        controller: 'employeeEditController'
      })

      // default route (employee list)
      .otherwise({
        redirectTo: '/'
      });
}