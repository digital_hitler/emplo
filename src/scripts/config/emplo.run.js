'use strict';

/**
 * App run configuration
 * @param $rootScope          link to root scope
 * @param localStorageService link to localStorage data service
 */

module.exports = function($rootScope, localStorageService) {
  $rootScope.windowTitle = 'Emplo';
  $rootScope.defaultData = require('../models/employeeList').defaultData;

  if(localStorageService.isSupported) {
    console.log('localStorage supported!');
  } else {
    console.warn('Your browser doesn\'t support localStorage. Please use newer one.')
  }
};