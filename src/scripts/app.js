'use strict';

/**
 * Emplo app
 * (c) Sergey S Petrenko <spetrenko@me.com>
 * Application entry point
 */

// Bootstrap-script:
var appLoader = require('./loader');

// Moment.js
var moment = require('moment');
moment.locale('ru');


/**
 * App dependencies & bootstrapping:
 */
var app = angular.module("emplo", [
  'LocalStorageModule', // localStorage provider
  'ngRoute' // router (see config/emplo.js for routes definitions)
])

    // Config & routes:
    .config(appLoader.emploConfig)
    .run(appLoader.emploRun)

    // Data storage:
    .service('employeeListLocalStorageService', appLoader.employeeListLocalStorageService)
    .provider('employeeListLocalStorageProvider', appLoader.employeeListLocalStorageProvider)

    // MVC:
    .controller('employeeListController', appLoader.employeeListController)
    .controller('employeeCreateController', appLoader.employeeCreateController)
    .controller('employeeEditController', appLoader.employeeEditController)
    .directive('employeeList', ["employeeListLocalStorageService", appLoader.employeeListDirective]);

// @todo: remove it if not used
$(document).ready(function() {
  console.log('DOM ready');
});