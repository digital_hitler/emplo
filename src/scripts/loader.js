'use strict';

/**
 * Loaders script
 * Used to bootstrap all application dependencies to one object with predefined values
 */

module.exports = {
  
  // Configurations:
  emploConfig: require('./config/emplo'),
  emploRun: require('./config/emplo.run'),

  // LocalStorage module
  localStorage: require('angular-local-storage'),

  // Employee schema-based model
  Employee: require('./models/Employee'),

  // Employee list Model, View (Directive) & Controller:
  employeeListModel: require('./models/employeeList'),
  employeeListController: require('./controllers/employeeList'),
  employeeListDirective: require('./directives/employeeList'),

  // Create & update employee controllers
  employeeCreateController: require('./controllers/employeeCreate'),
  employeeEditController: require('./controllers/employeeEdit'),

  // Service & provider for localStorage:
  employeeListLocalStorageService: require('./services/employeeListLocalStorage'),
  employeeListLocalStorageProvider: require('./providers/employeeListLocalStorage')
};


