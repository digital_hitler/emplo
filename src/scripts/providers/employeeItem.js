'use strict';


/**
 * Employee item provider
 * @todo: remove if not used
 */
module.exports = function() {

  /**
   *
   * @type {*[]}
   */
  this.$get = [
    "employeeListLocalStorageService",
    function employeeListLocalStorageFactory(employeeListLocalStorageService) {
      return employeeListLocalStorageService.getList();
    }];
};