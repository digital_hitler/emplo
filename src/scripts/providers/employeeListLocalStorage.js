module.exports = function() {
  this.isLoaded = true;

  this.$get = [
    "employeeListLocalStorageService",
    function employeeListLocalStorageFactory(employeeListLocalStorageService) {
      return employeeListLocalStorageService.getList();
    }];

  
};