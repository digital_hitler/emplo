'use strict';
/**
 * Employee list model & related things
 */

// Import Employee class (scheme based)
let Employee = require('./Employee');


/**
 * Generate random number in range
 * @param min minimal number
 * @param max maximal number
 * @returns {number} generated number
 */
function randomNumberFromInterval (min,max)
{
  return Math.floor(Math.random()*(max - min + 1) + min);
}

/**
 * Generates default data for app if no data detected in localStorage
 * @returns {{object}} array of {object|Employee} items
 */
function generateDefaultData() {

  let result = {};

  let employees = [
    [ 'Григорий', 'Лепс', 'Ораниенбаумская ул., д. 32, кв. 18', '12.12.1956'],
    [ 'Леонид', 'Агутин', 'Большой пр. П.С. д. 105, кв. 54', '22.01.1979' ],
    [ 'Елена', 'Степаненко', 'Купчинская ул., д. 15, кв. 999', '13.05.1992' ],
    [ 'Евгений', 'Петросян', 'пос. Шушары, ул. Крайняя, д. 90', '31.12.1999' ],
    [ 'Михаил', 'Задорнов', '10032, 25th Ave 351, apt. 1903, New York, NY', '19.04.1994' ]
  ];


  for(let curr in employees) {
    let newEmployee = new Employee({
      firstName:  employees[curr][0],
      lastName:   employees[curr][1],
      address:    employees[curr][2],
      dateBirth:  employees[curr][3],
      dateJoined: randomNumberFromInterval(1412000000, 1443893453)
    });

    result[newEmployee.id] = newEmployee;
  }

  return result;
}

// Exports:
module.exports = {
  modelStored: 'employeeList',
  defaultData: {
    items: generateDefaultData()
  },
  itemModel: Employee,
};