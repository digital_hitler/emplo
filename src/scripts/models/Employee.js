/**
 * Employee class
 * Implements Employee instance (entity)
 * Checks for schema, fields data, generates unique ID (RFC4122 v4) etc.
 * @type {Entity}
 */

let Entity  = require('../utils/Entity');
let moment  = require('moment');

class Employee extends Entity {

  constructor(fields) {
    super(fields, {
      'id':         [ 'string', 'required', function() { return Employee.generateUuid() } ],
      'firstName':  [ 'string', 'required' ],
      'lastName':   [ 'string', 'required' ],
      'dateJoined': [ 'date',   'required', function() { return Math.floor((Date.now() / 1000) - (3600 * 24 * 30 * 15)); } ],
      'dateBirth':  [ 'date',   'required', function() { return Math.floor((Date.now() / 1000) - (3600 * 24 * 30 * 12 * 25)); } ],
      'address':    [ 'string', 'optional', '' ]
    });
  }

  /**
   * RFC4122-based (version 4) Unique User ID generator
   * @returns {string} UUID
   */
  static generateUuid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
  }

  /**
   * Getter for fullName property
   * @returns {string} firstName & lastName
   */
  get fullName() {
    return this.firstName + ' ' + this.lastName;
  }

  /**
   * Formatted DoB: getter
   * Attention: we cannot use Moment.js with dates before year 1970
   * because of it's the year of "UNIX Epoch" beginning.
   * Trying to workaround, needs to be tested, may be buggy
   *
   * @todo Find another way
   * @returns {string} Date of birth
   */
  get age() {
    let birthFull = this.dateBirth.split('.');
    let birth = parseInt(birthFull[2]);
    return (new Date().getFullYear()) - birth;


  }

  /**
   * Formatted join date: getter
   * @returns {string} formatted string
   */
  get dateJoinedFormatted() {
    let momentDate = moment(new Date(this.dateJoined * 1000)) || Date.now();
    return momentDate.format('DD.MM.YYYY');
  }

  /**
   * Formatted amount of years and months in company: getter
   * @returns {string} formatted string
   */
  get dateJoinedDiff () {
    let a = moment(Date.now());
    let yearsIn =  a.diff(moment(new Date(this.dateJoined * 1000)), 'years');
    let monthsIn = (a.diff(moment(new Date(this.dateJoined * 1000)), 'months')) - (yearsIn * 12);
    let result = '';
    if(yearsIn > 0) result = yearsIn + ' ' + ( yearsIn === 1 ? ' год' : 'лет');
    if(monthsIn !== 0 || (yearsIn === 0)) result = result + ' ' + monthsIn + ' мес.';
    return result;
  }

}

module.exports = Employee;
