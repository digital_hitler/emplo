'use strict';
/**
 * Employee list directive
 * Usage: <employee-list />
 * @returns {{restrict: string, templateUrl: string}}   directive object
 */
module.exports = function() {
  return {
    restrict: 'E', templateUrl: '/templates/employeeList.html'
  }
};