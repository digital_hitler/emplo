/* eslint no-console: 0 */
/**
 * Emplo app
 * https://bitbucket.org/digital_hitler/emplo
 * (c) Sergey S Petrenko <spetrenko@me.com>
 *
 * @file server.js
 * @fileOverview Express-based primitive HTTP-server with webpack hot-update made to host the app.
 */

'use strict';

// * Required modules
const path = require('path');
const express              = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackConfig        = require('./webpack.config');

// * Configuration:
const port = process.env.EMPLOYERSAPP_PORT || 3344;


const app = new express();
const compiler = webpack(webpackConfig);


// * Loading express middlewares:
app.use(webpackDevMiddleware(compiler, {
  noInfo: true,
  publicPath: webpackConfig.output.publicPath,
  
}));
app.use(webpackHotMiddleware(compiler));
app.use(express.static(__dirname + '/dist'));

// * Routing root GET queries to index.html
app.get('/', function response(req, res) {
  res.write(middleware.fileSystem.readFileSync(path.join(__dirname, 'dist/index.html')));
  res.end();
});

// * Binding server to the tcp port:
app.listen(port, 'localhost', function onStart(err) {
  if (err) {
    console.error(
        '[!] Something goes wrong:' +
        err);
  }

  console.info(
      '==> Listening on port %s.\n\n' +
      'Open http://localhost:%s/ in your browser.\n\n' +
      'Press CTRL+C to stop everything.',
      port, port
  );
});