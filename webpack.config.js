'use strict';

var path    = require('path');
var webpack = require('webpack');

module.exports = {
  entry: [
      'webpack-hot-middleware/client',
      './src/scripts/app'
  ],

  output: {
    path: path.join(__dirname, 'dist', 'assets'),
    filename: 'app.js',
    publicPath: '/assets'
  },

  plugins: [
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoErrorsPlugin()
  ],

  module: {
    loaders: [
      {
        loader: 'babel-loader',
        test: '.js',
        query: {
          presets: 'es2015',
        },
      }
    ]
  }
};
